﻿using System;

namespace TicTacToe
{
    public static class TicTacToe
    {
        static char Mark = 'X';

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Title = "Крестики-Нолики";
                DrawMainMenu();
                int menuNumber = InputMenuNumber();
                switch (menuNumber)
                {
                    case 1:
                        Game.StartGame(Mark);
                        break;
                    case 2:
                        Options();
                        break;
                    case 3:
                        return;
                }
            }
        }

        public static int InputMenuNumber()
        {
            while (true)
            {
                int result;
                string input = Console.ReadLine();
                if (int.TryParse(input, out result) && result >= 1 && result <= 3)
                {
                    return result;
                }
                else
                {
                    DrawMainMenu();
                    Console.WriteLine("Вы ввели некорректные данные");
                    Console.WriteLine("Необходимо ввести число от 1 до 3");
                }
            }
        }

        public static void DrawMainMenu()
        {
            Console.Clear();
            Console.WriteLine("\tКрестики-нолики\n");
            Console.WriteLine("1. Начать игру");
            Console.WriteLine("2. Настройки");
            Console.WriteLine("3. Выйти из игры");
            Console.WriteLine("\nДля выбора пункта меню введите его номер\n");
        }

        public static void DrawOptions()
        {
            Console.Clear();
            Console.WriteLine("Выберите форму метки");
            Console.WriteLine("Текущая форма: {0}", ReturnCurrentMark());
            Console.WriteLine("Для выхода в главное меню введите back\n");
        }

        public static void Options()
        {
            DrawOptions();
            while (true)
            {
                string input = Console.ReadLine();
                switch (input)
                {
                    case "нолик":
                        Mark = 'O';
                        DrawOptions();
                        break;
                    case "крестик":
                        Mark = 'X';
                        DrawOptions();
                        break;
                    default:
                        DrawOptions();
                        Console.WriteLine("Вы ввели некорректные данные");
                        Console.WriteLine("Необходимо ввести 'крестик' или 'нолик' без кавычек");
                        break;
                }
                if (input == "back")
                {
                    return;
                }
            }
        }

        public static string ReturnCurrentMark()
        {
            if (Mark == 'X')
            {
                return "крестик";
            }
            else
            {
                return "нолик";
            }
        }
    }
}
