﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;

namespace TicTacToe
{
    public static class Game
    {
        static List<Combination> FirstPlayerCombinations;

        static List<Combination> SecondPlayerCombinations;

        public static void StartGame(char mark)
        {
            Console.Clear();
            FirstPlayerCombinations = new List<Combination>();
            SecondPlayerCombinations = new List<Combination>();
            int size = InputFieldSize();
            if (size == 0)
            {
                return;
            }

            Cell[,] field = new Cell[size, size];
            SetCoordinates(field, size);
            int gameMode = InputGameMode();
            if (gameMode == 0)
            {
                return;
            }

            int counter = 0;
            if (gameMode == 2)
            {
                while (true)
                {
                    PerformPlayerStep(field, size, mark);
                    mark = SwapMark(mark);
                    counter++;
                    PerformPlayerStep(field, size, mark);
                    mark = SwapMark(mark);
                    counter++;
                    if (CheckWinner(size, counter))
                    {
                        Thread.Sleep(3000);
                        return;
                    }
                }
            }
            else
            {
                while (true)
                {
                    PerformPlayerStep(field, size, mark);
                    mark = SwapMark(mark);
                    counter++;
                    PerformBotStep(field, size, mark);
                    mark = SwapMark(mark);
                    counter++;
                    if (CheckWinner(size, counter))
                    {
                        Thread.Sleep(3000);
                        return;
                    }
                }
            }
        }

        private static void SetCoordinates(Cell[,] field, int size)
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    field[i, j].X = i;
                    field[i, j].Y = j;
                    field[i, j].Figure = ' ';
                }
            }
        }

        private static int InputFieldSize()
        {
            Console.WriteLine("Введите размер поля (нечетное число)\n");
            while (true)
            {
                int result;
                string input = Console.ReadLine();
                if (input == "back")
                {
                    return 0;
                }
                else
                    if (int.TryParse(input, out result) && result % 2 == 1 && result >= 3)
                {
                    Console.Clear();
                    return result;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Введите размер поля (нечетное число)\n");
                    Console.WriteLine("Вы ввели некорректные данные");
                    Console.WriteLine("Необходимо ввести нечетное число");
                    Console.WriteLine("Для выхода в меню введите back");
                }
            }
        }

        private static int InputGameMode()
        {
            DrawGameModeMenu();
            while (true)
            {
                int result;
                string input = Console.ReadLine();
                if (input == "back")
                {
                    return 0;
                }
                else
                    if (int.TryParse(input, out result) && result >= 1 && result <= 2)
                {
                    Console.Clear();
                    return result;
                }
                else
                {
                    DrawGameModeMenu();
                    Console.WriteLine("\nВы ввели некорректные данные");
                    Console.WriteLine("Необходимо ввести число от 1 до 2");
                }
            }

        }

        private static void DrawGameModeMenu()
        {
            Console.Clear();
            Console.WriteLine("Выберите режим игры:");
            Console.WriteLine("1. Против компьютера");
            Console.WriteLine("2. Против игрока");
            Console.WriteLine("Для выхода в главное меню введите back\n");
        }

        private static char SwapMark(char mark)
        {
            if (mark == 'X')
            {
                mark = 'O';
            }
            else
            {
                mark = 'X';
            }

            return mark;
        }

        private static void DrawField(Cell[,] field, int size)
        {
            Console.Clear();
            for (int i = 0; i < size * 2 - 1; i++)
            {
                for (int j = 0; j < size * 2 - 1; j++)
                {
                    if (i % 2 == 1)
                    {
                        Console.Write("-");
                    }
                    else
                        if (j % 2 == 0)
                    {
                        Console.Write(field[(i + 1) / 2, (j + 1) / 2].Figure);
                    }
                    else
                    {
                        Console.Write("|");
                    }
                }

                Console.WriteLine();
            }
        }

        private static void PerformPlayerStep(Cell[,] field, int size, char mark)
        {
            char cursor = SetCursor(mark);
            int i = 0;
            int j = 0;
            CheckFirstElement(field, size, ref i, ref j);
            field[i, j].Figure = cursor;
            while (true)
            {
                Console.Clear();
                DrawField(field, size);
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.UpArrow:
                        i = CheckAndMoveUp(field, i, j);
                        break;
                    case ConsoleKey.DownArrow:
                        i = CheckAndMoveDown(field, size, i, j);
                        break;
                    case ConsoleKey.LeftArrow:
                        j = CheckAndMoveLeft(field, i, j);
                        break;
                    case ConsoleKey.RightArrow:
                        j = CheckAndMoveRight(field, size, i, j);
                        break;
                    case ConsoleKey.Enter:
                        field[i, j].Figure = mark;
                        SetCombinationsToPlayer(field, mark);
                        return;
                }

                field[i, j].Figure = cursor;
            }

        }

        private static bool CheckWinner(int size, int counter)
        {
            int firstPlayerPoints = FirstPlayerCombinations.Count;
            int secondPlayerPoints = SecondPlayerCombinations.Count;
            if (counter == Math.Pow(size, 2) - 1)
            {
                if (firstPlayerPoints > secondPlayerPoints)
                {
                    DrawWinnerMenu("крестики", firstPlayerPoints);
                    return true;
                }
                else
                    if (secondPlayerPoints > firstPlayerPoints)
                {
                    DrawWinnerMenu("нолики", secondPlayerPoints);
                    return true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Ничья!");
                    return true;
                }
            }

            return false;
        }

        private static void DrawWinnerMenu(string player, int points)
        {
            Console.Clear();
            Console.WriteLine("Победили {0}!\n", player);
            Console.WriteLine("Количество набранных очков: {0}", points);
        }

        private static void SetCombinationsToPlayer(Cell[,] field, char mark)
        {
            if (mark == 'X')
            {
                CombinationFiller.FillListWithCombinations(FirstPlayerCombinations, field, 'X');
            }
            
            if (mark == 'O')
            {
                CombinationFiller.FillListWithCombinations(SecondPlayerCombinations, field, 'O');
            }
        }

        private static char SetCursor(char mark)
        {
            if (mark == 'X')
            {
                return 'x';
            }
            else
            {
                return 'o';
            }
        }

        private static void PerformBotStep(Cell[,] field, int size, char mark)
        {
            Random randomNumber = new Random();
            while (true)
            {
                int i = randomNumber.Next(0, size);
                int j = randomNumber.Next(0, size);
                if (field[i, j].Figure == ' ')
                {
                    field[i, j].Figure = mark;
                    SetCombinationsToPlayer(field, mark);
                    return;
                }
            }
        }

        private static int CheckAndMoveUp(Cell[,] field, int i, int j)
        {
            if (i <= 0)
            {
                i = 0;
            }
            else
                if (field[i - 1, j].Figure != ' ')
            {
                return i;
            }
            else
            {
                field[i, j].Figure = ' ';
                i--;
            }

            return i;
        }

        private static int CheckAndMoveLeft(Cell[,] field, int i, int j)
        {
            if (j <= 0)
            {
                j = 0;
            }
            else
               if (field[i, j - 1].Figure != ' ')
            {
                return j;
            }
            else
            {
                field[i, j].Figure = ' ';
                j--;
            }

            return j;
        }

        private static int CheckAndMoveDown(Cell[,] field, int size, int i, int j)
        {
            if (i == size - 1)
            {
                i = size - 1;
            }
            else
                if (field[i + 1, j].Figure != ' ')
            {
                return i;
            }
            else
            {
                field[i, j].Figure = ' ';
                i++;
            }

            return i;
        }

        private static int CheckAndMoveRight(Cell[,] field, int size, int i, int j)
        {
            if (j == size - 1)
            {
                j = size - 1;
            }
            else
                if (field[i, j + 1].Figure != ' ')
            {
                return j;
            }
            else
            {
                field[i, j].Figure = ' ';
                j++;
            }

            return j;
        }

        private static void CheckFirstElement(Cell[,] field, int size, ref int i, ref int j)
        {
            for (i = 0; i < size; i++)
            {
                for (j = 0; j < size; j++)
                {
                    if (field[i, j].Figure == ' ')
                    {
                        return;
                    }
                }
            }
        }
    }
}
