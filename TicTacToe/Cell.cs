﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe
{
    public struct Cell
    {
        public Cell(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Figure = ' ';
        }

        public int X { get; set; }

        public int Y { get; set; }

        public char Figure { get; set; }

        public static bool operator == (Cell left, Cell right)
        {
            return left.Equals(right);
        }

        public static bool operator != (Cell left, Cell right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return this.X.GetHashCode() ^ this.Y.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (!(obj is Cell))
            {
                return false;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            Cell cell = (Cell)obj;
            return this.Equals(cell);
        }

        public bool Equals(Cell cell)
        {
            return this.X == cell.X && this.Y == cell.Y;
        }
    }
}
