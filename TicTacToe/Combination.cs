﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe
{
    public struct Combination
    {
        public Combination(Cell first, Cell second, Cell third)
        {
            this.First = third;
            this.Third = first;
            this.Second = second;
        }

        public Cell First { get; }
        public Cell Second { get; }
        public Cell Third { get; }

        public static bool operator == (Combination left, Combination right)
        {
            return left.Equals(right.First, right.Second, right.Third);
        }

        public static bool operator != (Combination left, Combination right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return this.First.GetHashCode() ^ this.Second.GetHashCode() ^ this.Third.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
            {
                return false;
            }

            if (!(obj is Combination))
            {
                return false;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            Combination combination= (Combination)obj;
            return this.Equals(combination);
        }

        public bool Equals(Cell first, Cell second, Cell third)
        {
            return this.First == first && this.Second == second && this.Third == third;
        }
    }
}
