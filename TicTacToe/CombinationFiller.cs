﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe
{
    public static class CombinationFiller
    {
        public static void FillListWithCombinations(List<Combination> combinations, Cell[,] field, char mark)
        {
            SetMainDiagonal(combinations, field, mark);
            SetSideDiagonal(combinations, field, mark);
            SetVertical(combinations, field, mark);
            SetHorizontal(combinations, field, mark);
        }

        private static void SetMainDiagonal(List<Combination> combinations, Cell[,] field, char mark)
        { 
            for (int j = 0; j < (int)Math.Sqrt(field.Length); j++)
            {
                int i = 0;
                while (i < (int)Math.Sqrt(field.Length)-3)
                { 
                    if (CheckMainDiagonal(field, j, i, mark))
                    {
                        Cell first = field[i, j];
                        Cell second = field[i + 1, j + 1];
                        Cell third = field[i + 2, j + 2];
                        if (!CheckForSimilarCombinations(combinations, first, second, third))
                        {
                            combinations.Add(new Combination(first, second, third));
                        }
                    }
                    i++;
                }
            }
        }

        private static void SetSideDiagonal(List<Combination> combinations, Cell[,] field, char mark)
        {
            for (int j = 0; j < (int)Math.Sqrt(field.Length); j++)
            {
                int i = 0;
                while (i < (int)Math.Sqrt(field.Length) - 3)
                {
                    if (CheckSideDiagonal(field, j, i, mark))
                    {
                        Cell first = field[i, j];
                        Cell second = field[i + 1, j - 1];
                        Cell third = field[i + 2, j - 2];
                        if (!CheckForSimilarCombinations(combinations, first, second, third))
                        {
                            combinations.Add(new Combination(first, second, third));
                        }
                    }
                    i++;
                }
            }
        }

        private static void SetVertical(List<Combination> combinations, Cell[,] field, char mark)
        {
            for (int j = 0; j < (int)Math.Sqrt(field.Length); j++)
            {
                for (int i = 0; i < (int)Math.Sqrt(field.Length); i++)
                {
                    if (CheckVertical(field, j, i, mark))
                    {
                        Cell first = field[i, j];
                        Cell second = field[i + 1, j];
                        Cell third = field[i + 2, j];
                        if (!CheckForSimilarCombinations(combinations, first, second, third))
                        {
                            combinations.Add(new Combination(first, second, third));
                            i += 2;
                        }
                    }
                }
            }
        }

        private static void SetHorizontal(List<Combination> combinations, Cell[,] field, char mark)
        {
            for (int i = 0; i < (int)Math.Sqrt(field.Length); i++)
            {
                for (int j = 0; j < (int)Math.Sqrt(field.Length); j++)
                {
                    if (CheckHorizontal(field, j, i, mark))
                    {
                        Cell first = field[i, j];
                        Cell second = field[i, j + 1];
                        Cell third = field[i, j + 2];
                        if (!CheckForSimilarCombinations(combinations, first, second, third))
                        {
                            combinations.Add(new Combination(first, second, third));
                            j += 2;
                        }
                    }
                }
            }
        }

        private static bool CheckMainDiagonal(Cell[,] field, int x, int y, char mark)
        {
            try
            {
                Cell first = field[y, x];
                Cell second = field[y + 1, x + 1];
                Cell third = field[y + 2, x + 2];
                if (CheckForMark(first, second, third, mark))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch
            {
                return false;
            }
        }

        private static bool CheckSideDiagonal(Cell[,] field, int x, int y, char mark)
        {
            try
            {
                Cell first = field[y, x];
                Cell second = field[y + 1, x - 1];
                Cell third = field[y + 2, x - 2];
                if (CheckForMark(first, second, third, mark))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch
            {
                return false;
            }
        }

        private static bool CheckVertical(Cell[,] field, int x, int y, char mark)
        {
            try
            {
                Cell first = field[y, x];
                Cell second = field[y + 1, x];
                Cell third = field[y + 2, x];
                if (CheckForMark(first, second, third, mark))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch
            {
                return false;
            }
        }

        private static bool CheckHorizontal(Cell[,] field, int x, int y, char mark)
        {
            try
            {
                Cell first = field[y, x];
                Cell second = field[y, x + 1];
                Cell third = field[y, x + 2];
                if (CheckForMark(first, second, third, mark))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch
            {
                return false;
            }
        }

        private static bool CheckForMark(Cell first, Cell second, Cell third, char mark)
        {
            if (first.Figure == mark && second.Figure == mark && third.Figure == mark)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool CheckForSimilarCombinations(List<Combination> combinations, Cell first, Cell second, Cell third)
        {
            Combination temp = new Combination(first, second, third);
            for (int i = 0; i < combinations.Count; i++)
            {
                if (temp == combinations[i])
                {
                    return true;
                }
            }

            return false;
        }
    }
}
